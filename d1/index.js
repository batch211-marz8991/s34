

const express = require("express");
/*
	- create an aplication using express
*/
const app = express()

const port = 3000;

//MIDDLEWARES

app.use(express.json());
/*
	-allows the app to read the data from forms

*/

app.use(express.urlencoded({extended: true}));
//ROUTES
/*
	-express has method corresponding to each HTTP method
	-this route express to recieve a GET request at the base URI
	- the full base URL from our local application for this route will be at "http://localhost:3000"
*/

/*
	-Tells our server to listen to the port
	-if the port is accessed, we can run the server
	-returns the message to confirm that the server is running in the terminal

*/
app.listen(port, () => console.log(`Server running at port ${port}`))

//RETURN simple message
//-this route express to recieve a GET request at the base URI "/"
	//POSTMAN
	// url: http://localhost:3000/
	// method: GET
app.get('/', (request, response) => {
	response.send('Hello World')
})



app.get('/hello', (request, response) => {
	response.send('Hello from the"/hello" endpoint')
})


//return simple greeting

/*
 URI: /hello
 method: 'POST'


POSTMAN 
url:http://localhost:3000/hello
method: POST
body: raw + json
	{
		"firstName": "Edmar",
		"lastName": "Amado"
	}
*/

app.post('/hello', (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.lastName}! This is from the"/hello endpoint but with a post method`)
})

let users = []


/*
	POSTMAN
	url:http://localhost:3000/register
method: POST
body: raw + json
	{
		"username": "Edmar",
		"password": " "
	}
*/

app.post('/register', (request, response) => {
	if (request.body.username !== '' && request.body.password !== '') {
		response.send(`User ${request.body.username} successfully registered`)
		console.log(request.body)
	} else {
		response.send('please input BOTH username and password')
	}
})

//change password route

/*
	-this route expect to recieve a PUT request at the URI "/cahnge-password"
	-this this will update the password of the user


	URI: /change-password
	method: PUT


	POSTMAN:
	POSTMAN
	url:http://localhost:3000/change-password
method: PUT
body: raw + json
	{
		"username": "Edmar",
		"password": "edmar123"
	}
*/

app.put('/change-password', (request,response) => {

    let message;

    for (let i = 0; i < users.length; i++) {

        if (request.body.username == users[i].username) {
            users[i].password = request.body.password
            message = `User ${request.body.username}'s password has been updated!`
            
            break;

        } else {
            message = "User does not exist"
        }
    }
    response.send(message);
})